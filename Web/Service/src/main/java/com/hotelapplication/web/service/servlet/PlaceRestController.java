/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.service.servlet;

import com.hotelapplication.core.entity.PlaceEntity;
import com.hotelapplication.core.service.PlaceService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bilal
 */

@RestController
@RequestMapping("place")
public class PlaceRestController {
    
    @Autowired
    PlaceService placeService;
    
    
    @RequestMapping(value="/get/{id}",method = RequestMethod.GET)
    public PlaceEntity getPlace(@PathVariable Long id){
        return placeService.getPlaceById(id);
    }
    
    @RequestMapping(value="/bycategory/{catId}")
    public List<PlaceEntity> getPlacesByCategory(@PathVariable Long catId){
        return placeService.getPlacesByCategory(catId);
    }
    
    @RequestMapping(value="/add",method=RequestMethod.POST)
    public void addPlace(@RequestBody PlaceEntity place){
        placeService.addPlace(place);
    }
    
    @RequestMapping(value="/update",method=RequestMethod.POST)
    public void updatePlace(@RequestBody PlaceEntity place){
        placeService.updatePlace(place);
    }
    
    
    
    
}
