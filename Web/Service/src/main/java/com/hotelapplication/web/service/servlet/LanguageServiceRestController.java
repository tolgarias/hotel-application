/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.service.servlet;

import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.service.LanguageService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bilal
 */
@RestController
@RequestMapping("/language")
public class LanguageServiceRestController {
    @Autowired
    LanguageService languageService;
    
    @RequestMapping(value="/all",method = RequestMethod.GET)
    public List<LanguageEntity> getAllLanguages(){
        return languageService.getAllLanguages();
    }
    
}
