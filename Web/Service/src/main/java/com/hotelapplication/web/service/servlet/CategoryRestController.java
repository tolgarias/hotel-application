    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.service.servlet;

import com.hotelapplication.core.entity.CategoryEntity;
import com.hotelapplication.core.service.CategoryService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bilal
 */
@RestController
@RequestMapping("/category")
public class CategoryRestController {
    
    @Autowired
    CategoryService categoryService;
    
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public CategoryEntity getCategory(@PathVariable Long id,HttpServletRequest request,HttpServletResponse response){
        return categoryService.getCategory(id);
    }
    
    @RequestMapping(value="/all/{langId}/{cityId}",method = RequestMethod.GET)
    public List<CategoryEntity> getAllCategories(@PathVariable int langId,@PathVariable int cityId){
        return categoryService.getCategories(langId,cityId);
    }
    
    @RequestMapping(value="/add/{cityId}",method = RequestMethod.POST)
    public void addCategory(@PathVariable int cityId,HttpServletRequest request,HttpServletResponse response){
        String categoryName = request.getParameter("name");
        Long langId = Long.parseLong(request.getParameter("langId"));
        categoryService.addCategory(categoryName, langId,cityId);
    }
    
}
