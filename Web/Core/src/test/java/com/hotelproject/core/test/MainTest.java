/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelproject.core.test;

import com.hotelapplication.core.entity.CategoryEntity;
import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.service.CategoryService;
import com.hotelapplication.core.service.LanguageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author bilal
 */

@ContextConfiguration(locations = {
		"classpath:spring-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class MainTest{
   @Autowired
   private LanguageService langService;
   
   @Autowired
   private CategoryService catService;
   
   @Test
   public void hibernateTest(){
       LanguageEntity lang = langService.getLanguageById(1l);
       Assert.assertEquals(lang.getName(),"Turkce");
   }
   
   @Test
   public void addCategory(){
       int count = catService.getCategories(1,1).size();
       catService.addCategory("Deneme", 1l,1);
       int count2 = catService.getCategories(1,1).size();
       Assert.assertNotEquals(count2, count);
   }
   
   @Test 
   public void getCategory(){
       CategoryEntity entity = catService.getCategory(1l);
       Assert.assertNotNull(entity);
   }
   
   
}
