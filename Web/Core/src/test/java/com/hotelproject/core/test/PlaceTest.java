/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelproject.core.test;

import com.hotelapplication.core.entity.PlaceEntity;
import com.hotelapplication.core.service.PlaceService;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author bilal
 */

@ContextConfiguration(locations = {
		"classpath:spring-context.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PlaceTest {

    @Autowired
    PlaceService placeService;
    
    @Test
    public void addPlace(){
        
        int count1 = placeService.getPlaces(1).size();
        PlaceEntity place = new PlaceEntity();
        place.setCaption("test place 1");
        place.setCategoryId(1l);
        place.setLangugeId(1);
        place.setInfo("test info");
        place.setLongtitude("longtitude");
        place.setLatitude("latitude");
        placeService.addPlace(place);
        int count2 = placeService.getPlaces(1).size();
        Assert.assertNotEquals(count2,count1);
        
    }
    
    @Test
    public void getPlacesByCatId(){
        List<PlaceEntity> result = placeService.getPlacesByCategory(1l);
        Assert.assertNotNull(result);
    }
    
    @Test 
    public void updatePlace(){
         PlaceEntity place = placeService.getPlaceById(1l);
         place.setCaption("test place update");
         placeService.updatePlace(place);
         place = placeService.getPlaceById(1l);
         Assert.assertEquals(place.getCaption(), "test place update");
    }
}
