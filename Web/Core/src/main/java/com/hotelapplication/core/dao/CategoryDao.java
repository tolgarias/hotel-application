/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.CategoryEntity;
import java.util.List;

/**
 *
 * @author bilal
 */
public interface CategoryDao {
    
    public void addCategory(String name,Long langId,int cityId);
    public CategoryEntity getCategory(Long id);
    public List<CategoryEntity> getCategories(int langId,int cityId);
    public void deleteCategory(Long catId);
    public void updateCategory(Long catId,String catName);
    
}
