/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author bilal
 */

@Entity
@Table(name="places")
public class PlaceEntity {
    @Id
    @GeneratedValue
    private Long id;
    
    
    @Column(name="language_id")
    private int langugeId;
    
    @Column(name="caption")
    private String caption;
    
    @Column(name="info")
    private String info;
    
    @Column(name="longtitude")
    private String longtitude;
    
    @Column(name="latitude")
    private String latitude;
    
    @Column(name="info_url")
    private String infoURL;
    
    @Column(name="hotel_id")
    private Long hotelId;
    
    @Column(name="category_id")
    private Long categoryId;

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    
    public int getLangugeId() {
        return langugeId;
    }

    public void setLangugeId(int langugeId) {
        this.langugeId = langugeId;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getInfoURL() {
        return infoURL;
    }

    public void setInfoURL(String infoURL) {
        this.infoURL = infoURL;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
    
    
    
}
