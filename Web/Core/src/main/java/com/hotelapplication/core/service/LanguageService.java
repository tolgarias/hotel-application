/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.hibernate.HibernateSessionManager;
import java.util.List;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author bilal
 */

@Service("languageService")
public interface LanguageService {
    public void addLanguage(String name,String code);
    public LanguageEntity getLanguageById(Long id);
    public List<LanguageEntity> getAllLanguages();
}
