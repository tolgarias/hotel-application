/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.PlaceEntity;
import com.hotelapplication.core.hibernate.HibernateSessionManager;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bilal
 */
@Repository("placesDao")

public class PlaceDaoImp implements PlaceDao{

    @Override
    public void addPlace(PlaceEntity place) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(place);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public PlaceEntity getPlaceById(Long id) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from PlaceEntity where id = :id");
        query.setParameter("id", id);
        List<PlaceEntity> result = query.list();
        session.close();
        return result.get(0);
    }

    @Override
    public List<PlaceEntity> getPlaces(int languageId) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from PlaceEntity where language_id = :langId");
        query.setParameter("langId", languageId);
        List<PlaceEntity> result = query.list();
        session.close();
        return result;
    }

    @Override
    public List<PlaceEntity> getPlacesByCategory(Long categoryId) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from PlaceEntity where category_id= :catId");
        query.setParameter("catId", categoryId);
        List<PlaceEntity> result = query.list();
        session.close();
        return result;
    }

    @Override
    public void updatePlace(PlaceEntity place) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        session.beginTransaction();
        session.saveOrUpdate(place);
        session.getTransaction().commit();
        session.close();
    }
    
}
