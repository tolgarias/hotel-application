/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.dao.LanguageDao;
import com.hotelapplication.core.entity.LanguageEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bilal
 */
@Service("languageService")
public class LanguageServiceImp implements LanguageService{
    LanguageDao languageDao;
    
    @Autowired
    public LanguageServiceImp(LanguageDao languageDao){
        this.languageDao = languageDao;
    }

    @Override
    public void addLanguage(String name,String code){
       
    }
    @Override
    public LanguageEntity getLanguageById(Long id){
        return this.languageDao.getLanguageById(id);
    }

    @Override
    public List<LanguageEntity> getAllLanguages() {
        return this.languageDao.getList();
    }
}
