/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.dao.PlaceDao;
import com.hotelapplication.core.entity.PlaceEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bilal
 */
@Service("placeService")
public class PlaceServiceImp implements PlaceService{
    PlaceDao placeDao;
    
    @Autowired
    public PlaceServiceImp(PlaceDao placeDao){
        this.placeDao = placeDao;
    }
    
    @Override
    public void addPlace(PlaceEntity place) {
        this.placeDao.addPlace(place);
    }

    @Override
    public PlaceEntity getPlaceById(Long id) {
        return this.placeDao.getPlaceById(id);
    }

    @Override
    public List<PlaceEntity> getPlaces(int languageId) {
        return this.placeDao.getPlaces(languageId);
    }

    @Override
    public List<PlaceEntity> getPlacesByCategory(Long categoryId) {
        return this.placeDao.getPlacesByCategory(categoryId);
    }

    @Override
    public void updatePlace(PlaceEntity place) {
        this.placeDao.updatePlace(place);
    }
    
}
