/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.PlaceEntity;
import java.util.List;

/**
 *
 * @author bilal
 */
public interface PlaceDao {
    public void addPlace(PlaceEntity place);
    public PlaceEntity getPlaceById(Long id);
    public List<PlaceEntity> getPlaces(int languageId);
    public List<PlaceEntity> getPlacesByCategory(Long categoryId);
    public void updatePlace(PlaceEntity place);
    
}
