/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.hibernate.HibernateSessionManager;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bilal
 */
@Repository("languageDao")
public class LanguageDaoImp implements LanguageDao {

    @Override
    public void addLangauge(String name, String code) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        session.beginTransaction();
        LanguageEntity language = new LanguageEntity();
        language.setCode(code);
        language.setName(name);
        session.save(language);
        session.getTransaction().commit();
    }

    @Override
    public LanguageEntity getLanguageByCode(String code) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from LanguageEntity where code = :langCode");
        query.setParameter("langCode", code);
        List<LanguageEntity> languages = query.list();
        session.close();
        return languages.get(0);
    }

    @Override
    public List<LanguageEntity> getList() {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from LanguageEntity");
        List<LanguageEntity> languages = query.list();
        session.close();
        return languages;
    }

    @Override
    public LanguageEntity getLanguageById(Long id) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from LanguageEntity where id = :langId");
        query.setParameter("langId", id);
        List<LanguageEntity> languages = query.list();
        session.close();
        return languages.get(0);
    }

}
