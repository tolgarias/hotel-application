/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.entity.CityEntity;
import java.util.List;

/**
 *
 * @author bilal
 */
public interface CityService {
    public CityEntity getCityById(int id);
    public CityEntity getCityByCode(int code);
    public List<CityEntity> getCities();
}
