/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.dao.CityDao;
import com.hotelapplication.core.entity.CityEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bilal
 */

@Service("cityService")
public class CityServiceImp implements CityService{
    CityDao cityDao;
    
    @Autowired
    public CityServiceImp(CityDao cityDao){
        this.cityDao = cityDao;
    }
    
    
    @Override
    public CityEntity getCityById(int id) {
        return this.cityDao.getCityById(id);
    }

    @Override
    public CityEntity getCityByCode(int code) {
        return this.cityDao.getCityByCode(code);
    }

    @Override
    public List<CityEntity> getCities() {
        return this.cityDao.getCities();
    }
    
}
