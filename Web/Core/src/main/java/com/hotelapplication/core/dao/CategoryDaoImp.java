/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.CategoryEntity;
import com.hotelapplication.core.hibernate.HibernateSessionManager;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bilal
 */

@Repository("categoryDao")
public class CategoryDaoImp implements CategoryDao{

    @Override
    public void addCategory(String name, Long langId,int cityId) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        session.beginTransaction();
        CategoryEntity category = new CategoryEntity();
        category.setName(name);
        category.setLanguageId(langId.intValue());
        category.setCityId(cityId);
        
        session.save(category);
        session.getTransaction().commit();
    }

    @Override
    public CategoryEntity getCategory(Long id) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CategoryEntity where id= :catId");
        query.setParameter("catId", id);
        List<CategoryEntity> result = query.list();
        session.close();
        return result.get(0);
    }

    @Override
    public List<CategoryEntity> getCategories(int langId,int cityId) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CategoryEntity where language_id= :langId and city_id= :cityId");
        query.setParameter("langId", langId);
        query.setParameter("cityId", cityId);
        List<CategoryEntity> result = query.list();
        session.close();
        return result;
    }

    @Override
    public void deleteCategory(Long catId) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("delete from CategoryEntity where id= :catId");
        query.setParameter("catId", catId.longValue());
        query.executeUpdate();
        session.close();
    }

    @Override
    public void updateCategory(Long catId, String catName) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CategoryEntity where id= :catId");
        query.setParameter("catId", catId);
        List<CategoryEntity> result = query.list();
        
        CategoryEntity tmpCat = result.get(0);
        tmpCat.setName(catName);
        session.saveOrUpdate(tmpCat);
        
        session.close();
    }
    
}
