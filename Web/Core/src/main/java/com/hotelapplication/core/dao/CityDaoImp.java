/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.CityEntity;
import com.hotelapplication.core.hibernate.HibernateSessionManager;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bilal
 */

@Repository("cityDao")
public class CityDaoImp implements CityDao{

    @Override
    public CityEntity getCityById(int id) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CityEntity where id=:cityId");
        query.setParameter("cityId", id);
        List<CityEntity> cities = query.list();
        
        session.close();
        
        return cities.get(0);
    }

    @Override
    public CityEntity getCityByCode(int code) {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CityEntity where code=:cityCode");
        query.setParameter("cityCode", code);
        List<CityEntity> cities = query.list();
        session.close();
        return cities.get(0);
    }

    @Override
    public List<CityEntity> getCities() {
        Session session = HibernateSessionManager.getSessionFactory().openSession();
        Query query = session.createQuery("from CityEntity");
        List<CityEntity> cities = query.list();
        session.close();
        return cities;
    }
    
}
