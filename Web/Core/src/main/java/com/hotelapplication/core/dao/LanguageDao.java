/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.LanguageEntity;
import java.util.List;

/**
 *
 * @author bilal
 */
public interface LanguageDao {
    public void addLangauge(String name,String code);
    public LanguageEntity getLanguageByCode(String code);
    public List<LanguageEntity> getList();
    public LanguageEntity getLanguageById(Long id);
}
