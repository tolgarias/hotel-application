/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.dao;

import com.hotelapplication.core.entity.CityEntity;
import java.util.List;

/**
 *
 * @author bilal
 */
public interface CityDao {
    public CityEntity getCityById(int id);
    public CityEntity getCityByCode(int code);
    public List<CityEntity> getCities();
    
}
