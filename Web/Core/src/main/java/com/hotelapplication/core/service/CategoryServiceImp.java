/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.core.service;

import com.hotelapplication.core.dao.CategoryDao;
import com.hotelapplication.core.entity.CategoryEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author bilal
 */
@Service("categoryService")
public class CategoryServiceImp implements CategoryService{
    CategoryDao categoryDao;
    
    @Autowired
    public CategoryServiceImp(CategoryDao categoryDao){
        this.categoryDao = categoryDao;
    }
    
    @Override
    public void addCategory(String name, Long langId,int cityId) {
       this.categoryDao.addCategory(name, langId,cityId);
    }

    @Override
    public CategoryEntity getCategory(Long id) {
        return this.categoryDao.getCategory(id);
    }

    @Override
    public List<CategoryEntity> getCategories(int langId,int cityId) {
        return this.categoryDao.getCategories(langId,cityId);
    }

    @Override
    public void deleteCategory(Long catId) {
        this.categoryDao.deleteCategory(catId);
    }

    @Override
    public void updateCategory(Long catId, String catName) {
        this.categoryDao.updateCategory(catId, catName);
    }
    
}
