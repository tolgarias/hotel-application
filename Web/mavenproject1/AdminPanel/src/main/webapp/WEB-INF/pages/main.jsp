<%-- 
    Document   : mainmenu
    Created on : May 7, 2015, 3:34:55 PM
    Author     : bilal
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Hotel Application</title>
    </head>
    <body>
        <c:url value="/home/language" var="languageURL"></c:url>
        <c:url value="/home/category/all" var="categoryURL"></c:url>
        <c:url value="/home/place/all" var="placeURL"></c:url>

            <div style="padding-top:100px;padding-left:150px;width:75%;" >
                <div style="float:left;width:100%;margin-bottom: 50px;background-color: #f4f4f4">
                    <div style="margin-right:20px;float:left;">
                        <a href="${categoryURL}">Kategoriler</a>
                    </div>
                <div style="margin-right:20px;float:left;"><a href="${placeURL}">Mekanlar</a></div>

                <div style="float:right;">
                    <div style="margin-right:20px;float:left;font-weight:bold"> ${language.name}</div>
                    <div style="margin-right:20px;float:left;font-weight:bold"> ${city.name}</div>
                    <div style="float:right"><a href="${languageURL}">Degistir</a></div>
                </div>
            </div>
            <div style="float:left;width:100%">
                <tiles:insertAttribute name="page" ignore="true"/>
            </div>
        </div>   


    </body>
</html>
</html>
