<%-- 
    Document   : allplaces
    Created on : May 13, 2015, 10:32:04 PM
    Author     : tolgasaglam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:if test="${fn:length(places) > 0}">
    <ul>
                <c:forEach items="${places}" var="place">
                    <c:url value="/home/place/${place.id}" var="placeURL"></c:url>
                    <li><a href="${placeURL}">${place.caption}</a></li>    
                </c:forEach>
    </ul>
    <br /><br />
    
    
    
</c:if>
    
    <c:url value="/home/place/new" var="addplace"></c:url>
    <a href="${addplace}">Mekan Ekle</a>