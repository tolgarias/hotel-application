
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:url var="selectLanguage" value="/home/language/select"/>

<div style="float:center;width:75%;">
    <form:form action="${selectLanguage}" method="POST" commandName="lang" >
        <div style="float:left;width:100%">
            <div style="width:25%;float:left">Dil:</div>
            <div>
                <form:select path="id">
                    <form:options items="${langs}" itemLabel="name" itemValue="id"></form:options>
                </form:select>
            </div>
        </div>
        <div style="float:left;width:100%">
            <div style="width:25%;float:left">Sehir:</div>
            <div>
                <form:select path="code">
                    <form:options items="${cities}" itemLabel="name" itemValue="code"></form:options>
                </form:select>
            </div>
        </div>
        <div style="float:left;width:100%">
            <div style="width:25%;float:left"></div>
            <div style="float:right;">
                <form:button id="submit" name="submit" value="submit">Sec</form:button>
            </div>
        </div>
    </form:form>
</div>
