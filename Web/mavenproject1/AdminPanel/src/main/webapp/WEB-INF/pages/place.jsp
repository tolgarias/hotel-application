<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Place Page</title>
    </head>
    <body>
        
        <c:choose>
						<c:when test="${newPlace}">
                                                    <c:url var="updatePlace" value="/home/place/add"></c:url>
						</c:when>
						<c:otherwise>
								<c:url var="updatePlace" value="/home/place/update/${place.id}"></c:url>
						</c:otherwise>
					</c:choose>
        <form:form commandName="container" method="POST" action="${updatePlace}">
            
                <table>
                    <tr> 
                        <td>Kategori:</td>
                        <td>
                            <form:select path="category">
                            <form:options items="${categories}" itemLabel="name" itemValue="id"></form:options>
                            </form:select>
                        </td>
                    </tr>
                     <tr> 
                        <td>Mekan adi:</td>
                        <td><form:input id="caption" path="place.caption" ></form:input></td>
                    </tr>
                    <tr> 
                        <td>Aciklama:</td>
                        <td><form:textarea id="info" path="place.info" ></form:textarea></td>
                    </tr>
                    <tr> 
                        <td>Longtitude:</td>
                        <td><form:input id="longtitude" path="place.longtitude" ></form:input></td>
                    </tr>
                    
                    <tr> 
                        <td>Latitude</td>
                        <td><form:input id="latitude" path="place.latitude" ></form:input></td>
                    </tr>
                    
                    <tr> 
                        <td>URL:</td>
                        <td><form:input id="infoURL" path="place.infoURL" ></form:input></td>
                    </tr>
                    
                    <tr> 
                        <td></td>
                        <td><form:button id="submit" name="submit" value="submit">Gonder</form:button></td>
                    </tr>
                </table>
                
                
            
        </form:form>
    </body>
</html>
