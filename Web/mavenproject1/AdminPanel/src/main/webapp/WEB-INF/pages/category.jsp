
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Languages</title>
    </head>
    <body>
        <h1>Kategoriler</h1>

        <c:url var="addCategory" value="/home/category/add"/>
        <c:url var="deleteCategory" value="/home/category/delete"/>

        <form:form action="${deleteCategory}" method="POST" commandName="category" >
            <form:select path="id" multiple="true" size="5">
                <form:options items="${categories}" itemLabel="name" itemValue="id"></form:options>
            </form:select>
            <form:button id="submit" name="submit" value="submit">Sil</form:button>
        </form:form>
            <br /><br />
        <form:form action="${addCategory}" method="POST" commandName="category">
            Kategori Adi:
            <form:input id="name" path="name"></form:input>
            <form:button value="submit">Ekle</form:button>
        </form:form>   
    </body>
</html>
