/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.admin;

import com.hotelapplication.core.entity.CategoryEntity;
import com.hotelapplication.core.entity.CityEntity;
import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.entity.PlaceEntity;
import com.hotelapplication.core.service.CategoryService;
import com.hotelapplication.core.service.LanguageService;
import com.hotelapplication.core.service.PlaceService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author bilal
 */
@Controller
public class PlaceContoller {

    @Autowired
    PlaceService placeService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    LanguageService languageService;

    @RequestMapping(value = "/home/place/add", method = RequestMethod.POST)
    public ModelAndView addPlace(HttpServletRequest request, HttpServletResponse response) {
        LanguageEntity language = getLanguage(request);
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        PlaceEntity newPlace = getPlace(new PlaceEntity(), language, request);
        placeService.addPlace(newPlace);
        return displayPlace(language, city, newPlace, false);
    }

    @RequestMapping(value = "/home/place/new", method = RequestMethod.GET)
    public ModelAndView newPlace(HttpServletRequest request, HttpServletResponse response) {
        LanguageEntity language = getLanguage(request);
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        PlaceEntity newPlace = new PlaceEntity();
        return displayPlace(language, city, newPlace, true);
    }

    @RequestMapping(value = "/home/place/all", method = RequestMethod.GET)
    public ModelAndView getAllPlaces(HttpServletRequest request, HttpServletResponse response) {
        LanguageEntity language = getLanguage(request);
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        List<CategoryEntity> categories = categoryService.getCategories(language.getId().intValue(), city.getId());
        List<PlaceEntity> places = new ArrayList<>();
        
        for (CategoryEntity category : categories) {
            places.addAll(placeService.getPlacesByCategory(category.getId()));
        }
        ModelAndView model = new ModelAndView("allplaces");
        model.addObject("places", places);
        return model;
    }

    @RequestMapping(value = "/home/place/{placeId}", method = RequestMethod.GET)
    public ModelAndView getPlace(@PathVariable Long placeId, HttpServletRequest request, HttpServletResponse response) {
        LanguageEntity language = getLanguage(request);
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        PlaceEntity place = placeService.getPlaceById(placeId);
        return displayPlace(language, city, place, false);
    }

    @RequestMapping(value = "/home/place/update/{placeId}", method = RequestMethod.POST)
    public ModelAndView updatePlace(@PathVariable Long placeId, HttpServletRequest request, HttpServletResponse response) {
        LanguageEntity language = getLanguage(request);
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        PlaceEntity place = placeService.getPlaceById(placeId);
        place = getPlace(place, language, request);
        placeService.updatePlace(place);
        return displayPlace(language, city, place, false);
    }

    private LanguageEntity getLanguage(HttpServletRequest request) {
        LanguageEntity language = (LanguageEntity) request.getSession().getAttribute("selectedLanguage");
        if (language == null) {
            language = languageService.getLanguageById(1l);
        }
        return language;
    }

    private ModelAndView displayPlace(LanguageEntity language, CityEntity city, PlaceEntity place, boolean newPlace) {
        ModelAndView model = new ModelAndView("place");
        model.addObject("newPlace", newPlace);
        model.addObject("place", place);
        model.addObject("categories", categoryService.getCategories(language.getId().intValue(), city.getId()));

        PlaceCategoryContainer container = new PlaceCategoryContainer();
        container.setCategory(new CategoryEntity());
        container.setPlace(place);

        model.addObject("container", container);

        return model;
    }

    private PlaceEntity getPlace(PlaceEntity place, LanguageEntity language, HttpServletRequest request) {

        place.setCaption(request.getParameter("place.caption"));
        place.setInfo(request.getParameter("place.info"));
        place.setLongtitude(request.getParameter("place.longtitude"));
        place.setLatitude(request.getParameter("place.latitude"));
        place.setInfoURL(request.getParameter("place.infoURL"));
        place.setLangugeId(language.getId().intValue());
        place.setCategoryId(Long.parseLong(request.getParameter("category")));

        return place;
    }

    public class PlaceCategoryContainer {

        public PlaceCategoryContainer() {

        }
        private PlaceEntity place;
        private CategoryEntity category;

        public PlaceEntity getPlace() {
            return place;
        }

        public void setPlace(PlaceEntity place) {
            this.place = place;
        }

        public CategoryEntity getCategory() {
            return category;
        }

        public void setCategory(CategoryEntity category) {
            this.category = category;
        }

    }
}
