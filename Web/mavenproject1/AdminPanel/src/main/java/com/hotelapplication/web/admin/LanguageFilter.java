/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.admin;

import com.hotelapplication.core.entity.CityEntity;
import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.service.CityService;
import com.hotelapplication.core.service.LanguageService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author bilal
 */
public class LanguageFilter extends HandlerInterceptorAdapter{
    
    @Autowired
    LanguageService languageService;
    
    @Autowired
    CityService cityService;
   
    public boolean preHandle(
	            HttpServletRequest request,
	            HttpServletResponse response,
	            Object handler) throws Exception {
        LanguageEntity language = (LanguageEntity)request.getSession().getAttribute("selectedLanguage");
        if(language == null) {
            language = languageService.getLanguageById(1l);
        }
        request.getSession().setAttribute("selectedLanguage", language);
        
        
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        if(city==null){
            city = cityService.getCityByCode(7);
        }
        request.getSession().setAttribute("selectedCity", city);
                
        return true;
    }
    
    public void postHandle(
	            HttpServletRequest request,
	            HttpServletResponse response,
	            Object handler,ModelAndView model) throws Exception {
        LanguageEntity language = (LanguageEntity)request.getSession().getAttribute("selectedLanguage");
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        model.addObject("language",language);
        model.addObject("city", city);
    }
}
