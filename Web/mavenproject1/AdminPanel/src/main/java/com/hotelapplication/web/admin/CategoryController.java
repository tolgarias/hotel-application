/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.admin;

import com.hotelapplication.core.entity.CategoryEntity;
import com.hotelapplication.core.entity.CityEntity;
import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.service.CategoryService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author bilal
 */
@Controller
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping("/home/category/all")
    public ModelAndView getAllCategories(HttpServletRequest request,HttpServletResponse response) {
        LanguageEntity language = (LanguageEntity)request.getSession().getAttribute("selectedLanguage");
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        return  displayAllCategories(language,city);
    }
    public ModelAndView displayAllCategories(LanguageEntity language,CityEntity city){
        ModelAndView modAndView = new ModelAndView("category");
        List<CategoryEntity> categories = categoryService.getCategories(language.getId().intValue(),city.getId());
        CategoryEntity tmp = new CategoryEntity();
        modAndView.addObject("category",tmp);
        modAndView.addObject("categories",categories);
        return modAndView;
    }
    @RequestMapping(value = "/home/category/add",method = RequestMethod.POST)
    public ModelAndView addCategory(HttpServletRequest request,HttpServletResponse response){
        LanguageEntity language = (LanguageEntity)request.getSession().getAttribute("selectedLanguage");
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        String categoryName = request.getParameter("name");
        categoryService.addCategory(categoryName, language.getId(),city.getId());
        return  displayAllCategories(language,city);
    }
    
    @RequestMapping(value = "/home/category/delete",method = RequestMethod.POST)
    public ModelAndView delete(HttpServletRequest request,HttpServletResponse response){
        LanguageEntity language = (LanguageEntity)request.getSession().getAttribute("selectedLanguage");
        CityEntity city = (CityEntity) request.getSession().getAttribute("selectedCity");
        String[] catIds = request.getParameter("id").split(",");
        for(String s:catIds){
            Long catId = Long.parseLong(s);
            categoryService.deleteCategory(catId);
        }
        return  displayAllCategories(language,city);
    }
    
}
