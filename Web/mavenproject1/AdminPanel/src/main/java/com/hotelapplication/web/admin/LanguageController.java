/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hotelapplication.web.admin;

import com.hotelapplication.core.entity.CityEntity;
import com.hotelapplication.core.entity.LanguageEntity;
import com.hotelapplication.core.service.CityService;
import com.hotelapplication.core.service.LanguageService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author bilal
 */

@Controller
public class LanguageController {
    @Autowired
    public  LanguageService langService;
    
    @Autowired
    CityService cityService;
    
    @RequestMapping("/home/language")
    public ModelAndView getLanguages(HttpServletRequest request,HttpServletResponse response ){
        ModelAndView modAndView = new ModelAndView("lang");
        List<LanguageEntity> languages = langService.getAllLanguages();
        modAndView.addObject("langs", languages);
        modAndView.addObject("lang", new LanguageEntity());
        modAndView.addObject("cities", cityService.getCities());
        return modAndView;
    }
    
    @RequestMapping(value="/home/language/select",method = RequestMethod.POST)
    public ModelAndView selectLanguage(HttpServletRequest request,HttpServletResponse response){
        long id = Long.parseLong(request.getParameter("id"));
        int code = Integer.parseInt(request.getParameter("code"));
        
        LanguageEntity selectedLanguage = langService.getLanguageById(id);
        request.getSession().setAttribute("selectedLanguage", selectedLanguage);
        
        CityEntity city = cityService.getCityByCode(code);
        request.getSession().setAttribute("selectedCity", city);
        
        ModelAndView modAndView = new ModelAndView("lang");
        List<LanguageEntity> languages = langService.getAllLanguages();
        modAndView.addObject("langs", languages);
        modAndView.addObject("lang", new LanguageEntity());
        modAndView.addObject("cities", cityService.getCities());
        
        return modAndView;
    }
}
